# Notes

# Buys

## March

- [ ] 1 Mausoleum Wanderer
- [ ] 4 Mox Diamond *18.64
- [ ] 4 Aether Vial *7.08
- [ ] ? Standard Bearer *1.44
- [ ] 4 Spell Queller *2.80

## Wasn't reprinted

- [ ] 4 Life from the Loam *10 *15
- [x] 4 Nettle Sentinel =3.84 (17-03-03)
- [x] 4 Slippery Bogle =1.86 (17-03-03)
- [x] 2 Dismember =3.36 (17-03-03)
- [x] 4 Infernal Tutor=30 (17-03-03)
- [ ] 3 Grove of the Burnwillows *27
- [ ] 2 Surgical Extraction
- [x] 2 Rest in Peace =3.75 (17-03-03)
- [x] 4 Fatal Push =1.56 (17-04-06)

## April/May

- [ ] 4 Voice of Resurgence
- [ ] 4 Blood Moon
- [x] 2 Craterhoof Behemoth =2.16 (MM3, AVR is double) (17-04-06)
- [x] 2 Past in Flames =5.6 (17-03-04)
- [x] 4 Abrupt Decay =2.93 (17-03-04)
- [x] 4 Misty Rainforest =7.26 (17-03-04)
- [x] 4 Scalding Tarn =15.34 (17-04-06)
- [ ] 2 Arid Mesa
- [ ] 4 Verdant Catacombs
- [ ] 1 Marsh Flats
- [ ] 1 Snapcaster Mage
- [x] 4 Griselbrand =9.5 (MM3) (17-04-06)
- [ ] 4 Show and Tell
- [x] 1 Sphinx's Revelation =2.23 (17-04-06)
- [ ] 1 Sphinx's Revelation
- [ ] 4 Liliana of the Veil
- [x] 4 Grafdigger's Cage =3 (17-04-06)
- [x] 3 Path to Exile =1.09 (17-04-06)
- [ ] 1 Path to Exile
- [ ] 4 Inquisition of Kozilek
- [x] 2 Venser, Shaper Savant =1.12 (17-04-06)
- [ ] 2 Ranger of Eos
- [x] 2 Temporal Mastery =1.27 (MM3) (17-04-06)
- [ ] 2 Temporal Mastery
- [ ] 4 Damnation
- [ ] 4 Goblin Guide
- [x] 2 Restoration Angel =2.16 (17-04-06)
- [ ] 2 Restoration Angel
- [x] 4 Flickerwisp =0.23 (17-04-06)
- [x] 2 Scavenging Ooze =1.53 (MM3) (17-04-06)